import uuid


class Wallet:
    def __init__(self, balance=100):
        self.unique_id = self.generate_unique_id()
        self.balance = balance
        self.history = {}

    def __str__(self):
        return f"\n\t ID du Wallet : {self.unique_id} \n" \
               f"\t Balance : {self.balance} \n"

    @staticmethod
    def generate_unique_id():
        return str(uuid.uuid4())

    def add_balance(self):
        pass

    def sub_balance(self):
        pass

    def save_wallet(self):
        pass

    def load(self):
        pass


w1 = Wallet()
w2 = Wallet()
print(w1)
print(w2)
