

class Block:
    def __init__(self, unique_id, hash, parent_hash, transactions_list):
        self.unique_id = unique_id
        self.hash = hash
        self.parent_hash = parent_hash
        self.transactions_list = transactions_list

    def check_hash(self):
        pass

    def add_transaction(self):
        pass

    def get_transaction(self):
        pass

    def get_block_size(self):
        pass

    def save_block(self):
        pass

    def load_block(self):
        pass
